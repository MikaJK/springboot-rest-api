# springboot-rest-api

This is a template of a REST API implemented with Spring Boot framework.

The REST API shall implement GET, POST, PUT and DELETE methods for Duck objects.

This implementation shall not use a database, but a static data.