package com.pikecape.springbootrestapi.api;

import com.pikecape.springbootrestapi.model.Duck;
import com.pikecape.springbootrestapi.service.DuckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Component;

@RequestMapping("api/v1/duck")
@RestController
@Component
public class DuckController {

    private final DuckService duckService;

    @Autowired
    public DuckController(DuckService duckService) {
        this.duckService = duckService;
    }

    @PostMapping
    public Duck postDuck(@Valid @NotNull @RequestBody Duck duck) {
        return duckService.postDuck(duck);
    }

    @GetMapping
    public List<Duck> getDucks() {
        return duckService.getDucks();
    }

    @GetMapping(path = "{id}")
    public Duck getDuck(@PathVariable("id") UUID id) {
        return duckService.getDuck(id);
    }

    @PutMapping(path = "{id}")
    public Duck putDuck(@PathVariable("id") UUID id, @Valid @NotNull @RequestBody Duck duck) {
        return duckService.putDuck(id, duck);
    }

    @DeleteMapping(path = "{id}")
    public boolean deleteDuck(@PathVariable("id") UUID id) {
        return duckService.deleteDuck(id);
    }
}
