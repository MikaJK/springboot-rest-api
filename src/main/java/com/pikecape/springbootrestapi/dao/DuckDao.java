package com.pikecape.springbootrestapi.dao;

import com.pikecape.springbootrestapi.model.Duck;

import java.util.List;
import java.util.UUID;

public interface DuckDao {

    Duck createDuck(Duck duck);

    List<Duck> queryDucks();

    Duck queryDuck(UUID id);
    
    Duck updateDuck(UUID id, Duck duck);

    boolean deleteDuck(UUID id);
}