package com.pikecape.springbootrestapi.dao;

import com.pikecape.springbootrestapi.model.Duck;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository("fakeDao")
public class FakeDuckDataAccessService implements DuckDao {

    private static final List<Duck> DB = new ArrayList<>();

    @Override
    public Duck createDuck(Duck duck) {
        duck.setId(UUID.randomUUID());
        if (DB.add(duck)) {
            return duck;
        } else {
            return null;
        }
    }

    @Override
    public List<Duck> queryDucks() {
        return DB;
    }

    @Override
    public Duck queryDuck(UUID id) {
        
        for (Duck duck : DB) {
            if (duck.getId().equals(id)) {
                return duck;
            }
        }

        return null;
    }

    @Override
    public Duck updateDuck(UUID id, Duck duck) {
        Duck storedDuck = queryDuck(id);

        if (storedDuck == null) {
            return null;
        }

        int index = DB.indexOf(storedDuck);

        if (index >= 0) {
            
            if (DB.set(index, duck) != null) {
                return duck;
            }
        }

        return null;
    }

    @Override
    public boolean deleteDuck(UUID id) {
        Duck duck = queryDuck(id);

        if (duck == null) {
            return false;
        }

        return DB.remove(duck);
    }
}
