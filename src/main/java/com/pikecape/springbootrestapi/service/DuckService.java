package com.pikecape.springbootrestapi.service;

import com.pikecape.springbootrestapi.dao.DuckDao;
import com.pikecape.springbootrestapi.model.Duck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DuckService {

    private final DuckDao duckDao;

    @Autowired
    public DuckService(@Qualifier("fakeDao") DuckDao duckDao) {
        this.duckDao = duckDao;
    }

    public Duck postDuck(Duck duck) {
        return duckDao.createDuck(duck);
    }

    public List<Duck> getDucks() {
        return duckDao.queryDucks();
    }

    public Duck getDuck(UUID id) {
        return duckDao.queryDuck(id);
    }

    public Duck putDuck(UUID id, Duck duck) {
        return duckDao.updateDuck(id, duck);
    }

    public boolean deleteDuck(UUID id) {
        return duckDao.deleteDuck(id);
    }
}
