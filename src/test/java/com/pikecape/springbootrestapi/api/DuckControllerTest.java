package com.pikecape.springbootrestapi.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pikecape.springbootrestapi.model.Duck;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

/**
 * DuckControllerTest.
 * 
 * @author mika.korpela
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DuckControllerTest {
    
    public DuckControllerTest() {
    }
    
    @Autowired
    private WebApplicationContext wac;
    
    private MockMvc mockMvc;
    
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     * Test of postDuck method, of class DuckController.
     * @throws java.lang.Exception
     */
    @Test
    public void testPostDuck() throws Exception {
        System.out.println("postDuck");
        
        Duck duck = new Duck();
        duck.setFirstName("Duey");
        duck.setLastName("Duck");
        
        ObjectMapper objectMapper = new ObjectMapper();
        String duckJson = objectMapper.writeValueAsString(duck);
        
        MvcResult mvcResult = mockMvc.perform(post("/api/v1/duck").contentType(MediaType.APPLICATION_JSON).content(duckJson)).andExpect(status().isOk()).andReturn();
        Duck createdDuck = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Duck.class);
        assertNotNull(createdDuck);
        assertEquals(createdDuck.getFirstName(), duck.getFirstName());
        assertEquals(createdDuck.getLastName(), duck.getLastName());
    }

    /**
     * Test of getDucks method, of class DuckController.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetDucks() throws Exception {
        System.out.println("getDucks");
//        DuckController instance = null;
//        List<Duck> expResult = null;
//        List<Duck> result = instance.getDucks();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
        ResultActions resultActions = mockMvc.perform(get("/api/v1/duck").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
        System.out.println("RESULT ACTIONS: " + resultActions.andReturn().getResponse().getContentAsString());
    }

//    /**
//     * Test of getDuck method, of class DuckController.
//     */
//    @Test
//    public void testGetDuck() {
//        System.out.println("getDuck");
//        UUID id = null;
//        DuckController instance = null;
//        Duck expResult = null;
//        Duck result = instance.getDuck(id);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of putDuck method, of class DuckController.
//     */
//    @Test
//    public void testPutDuck() {
//        System.out.println("putDuck");
//        UUID id = null;
//        Duck duck = null;
//        DuckController instance = null;
//        Duck expResult = null;
//        Duck result = instance.putDuck(id, duck);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of deleteDuck method, of class DuckController.
//     */
//    @Test
//    public void testDeleteDuck() {
//        System.out.println("deleteDuck");
//        UUID id = null;
//        DuckController instance = null;
//        boolean expResult = false;
//        boolean result = instance.deleteDuck(id);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
}
